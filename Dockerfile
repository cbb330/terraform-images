ARG BASE

FROM golang:1.14 AS tfplantool

ARG BASE
ARG TFPLANTOOL

WORKDIR /tfplantool

RUN git clone --branch $TFPLANTOOL --depth 1 https://gitlab.com/mattkasa/tfplantool.git .
RUN sed -i -e "/github\.com\/hashicorp\/terraform/s/ v.*\$/ v$(echo "$BASE" | sed -e "s/^.*://")/" go.mod
RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tfplantool .

FROM $BASE

RUN apk add --no-cache jq python3 py-pip

COPY --from=tfplantool /tfplantool/tfplantool /usr/bin/tfplantool

COPY src/bin/gitlab-terraform.sh /usr/bin/gitlab-terraform
RUN chmod +x /usr/bin/gitlab-terraform


COPY requirements.txt /opt/app/requirements.txt
RUN pip install -r /opt/app/requirements.txt

ENV VERSION="3.5.2"
ENV BASE_HELM_URL="https://get.helm.sh"
ENV HELM_TAR_FILE="helm-v${VERSION}-linux-amd64.tar.gz"

RUN apk add --update --no-cache curl ca-certificates && \
    curl -L ${BASE_HELM_URL}/${HELM_TAR_FILE} | tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm
    # rm -rf linux-amd64 && \
    # apk del curl && \
    # rm -f /var/cache/apk/*

# Override ENTRYPOINT since hashicorp/terraform uses `terraform`
ENTRYPOINT []
